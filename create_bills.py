import sys
from openpyxl import Workbook, load_workbook
import calendar
from datetime import datetime
import os.path

sys.setrecursionlimit(10000)
current_year = datetime.now().year


def add_bill():
    month_validation()
    expensetype_validation()
    expense_validation()
    wb.save('bills_' + str(current_year) + '.xlsx')
    return


def month_validation():
    global i
    month_requested = input('Enter the month that you want to add the bill:\n')
    for i in range(2, ws.max_row + 1):
        if month_requested == ws.cell(i, 1).value:
            return
    else:
        month_validation()


def expensetype_validation():
    global j
    expense_requested = input('Enter the expense type that you want to add the bill:\n')
    for j in range(1, ws.max_column + 1):
        if expense_requested == ws.cell(1, j).value:
            return
    else:
        expensetype_validation()


def expense_validation():
    expense_value = input('Enter the amount that you spent:\n')
    print(f'You entered {expense_value}')
    if expense_value.isnumeric():
        ws.cell(i, j).value = expense_value
        return
    else:
        print("Error occurred value is not a number")
        expense_validation()


if os.path.isfile('bills_' + str(current_year) + '.xlsx'):
    print("File exist")
    wb = load_workbook('bills_' + str(current_year) + '.xlsx')
    ws = wb.active
    add_bill()
else:
    print("File not exists")
    wb = Workbook()
    ws = wb.active
    ws.title = "bills2020"
    ws['A1'].value = 'Month'
    ws['B1'].value = 'Supermarket'
    ws['C1'].value = 'Drinks'
    ws['D1'].value = 'Vacation'
    ws['E1'].value = 'Clothes'
    ws['F1'].value = 'Others'
    month_id = 1

    for i in ws.iter_rows(min_row=1, max_row=12):
        month = calendar.month_name[month_id]
        month_id = month_id + 1
        ws.append([month])
    add_bill()
